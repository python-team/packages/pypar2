# -*- coding: utf-8 -*-
#
# Author: Ingelrest François (Athropos@gmail.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import os.path

# Application
appVersion   = '1.4'
appURL       = 'http://pypar2.silent-blade.org'
appName      = 'PyPar2'
appNameShort = 'pypar2'
prefsVersion = 1

# Directories
dirUsr = os.path.expanduser('~')
dirCfg = os.path.join(dirUsr, '.' + appNameShort)
dirSrc = os.path.join(os.path.dirname(__file__))
dirRes = os.path.join(dirSrc, '..', 'res')
dirPix = os.path.join(dirSrc, '..', 'pix')
dirDoc = os.path.join(dirSrc, '..', 'doc')

dirLocale = os.path.join(dirSrc, '..', 'locale')
if not os.path.isdir(dirLocale) :
    dirLocale = os.path.join(dirSrc, '..', '..', 'locale')

# Files
filePrefs    = os.path.join(dirCfg, 'prefs.xml')
fileLicense  = os.path.join(dirDoc, 'gpl.txt')
fileImgIcon  = os.path.join(dirPix, 'icon.png')
fileImgAbout = os.path.join(dirPix, 'about.png')

# Create the configuration directory if needed
if not os.path.isdir(dirCfg) :
    os.mkdir(dirCfg)
