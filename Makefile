INSTALL ?= install
MAKE ?= make
RM ?= rm
RMDIR ?= rmdir
prefix ?= /usr/local


PREFIX = $(DESTDIR)$(prefix)

BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man/man1
DATADIR = $(PREFIX)/share/pypar2
SRCDIR = $(DATADIR)/src
PIXDIR = $(DATADIR)/pix
RESDIR = $(DATADIR)/res

APPDIR = $(PREFIX)/share/applications
ICONDIR = $(PREFIX)/share/pixmaps
LOCALEDIR = $(PREFIX)/share/locale

LANGUAGES = `find locale/ -maxdepth 1 -mindepth 1 -type d -printf "%f "`

help:
	@echo Usage:
	@echo "make		- not used"
	@echo "make clean	- removes temporary data"
	@echo "make install	- installs data"
	@echo "make uninstall	- uninstalls data"
	@echo "make help	- prints this help"
	@echo


install:
	echo $(PREFIX)
	$(INSTALL) -m 755 -d $(BINDIR) $(MANDIR) $(DATADIR) $(SRCDIR) $(PIXDIR) $(RESDIR) $(APPDIR) $(ICONDIR)
	$(INSTALL) -m 644 src/*.py $(SRCDIR)
	$(INSTALL) -m 644 res/*.glade $(RESDIR)
	$(INSTALL) -m 644 doc/pypar2.1 $(MANDIR)
	$(INSTALL) -m 644 pix/*.png $(PIXDIR)
	$(INSTALL) -m 644 pix/pypar2.png $(ICONDIR)
	$(INSTALL) -m 644 res/pypar2.desktop $(APPDIR)
	if test -L $(BINDIR)/pypar2; then ${RM} $(BINDIR)/pypar2; fi
	ln -s $(SRCDIR)/main.py $(BINDIR)/pypar2
	chmod +x $(SRCDIR)/main.py
#	$(MAKE) -C po dist
	for lang in $(LANGUAGES); do \
		${INSTALL} -m 755 -d $(LOCALEDIR)/$$lang/LC_MESSAGES;\
		$(INSTALL) -m 644 locale/$$lang/LC_MESSAGES/pypar2.mo $(LOCALEDIR)/$$lang/LC_MESSAGES/; \
	done


uninstall:
	${RM} $(PREFIX)/bin/pypar2
	${RM} $(APPDIR)/pypar2.desktop
	${RM} $(MANDIR)/pypar2.1
	${RM} $(ICONDIR)/pypar2.png
	${RM} -rf $(DATADIR)
	$(RMDIR) --ignore-fail-on-non-empty $(BINDIR) $(MANDIR) $(APPDIR) $(ICONDIR)
	for lang in $(LANGUAGES); do \
		${RM} $(LOCALEDIR)/$$lang/LC_MESSAGES/pypar2.mo; \
	done

clean:
	${RM} src/*.py[co] res/*~ res/*.bak

.PHONY: help clean install
